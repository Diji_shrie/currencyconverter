package com.opsurs.currencyconverter.Network

import Json4Kotlin_Base
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET


private val service: Network by lazy {
    val interceptor = HttpLoggingInterceptor()
    interceptor.level = HttpLoggingInterceptor.Level.BODY
    val okHttpClient = OkHttpClient.Builder()
        .addInterceptor(interceptor)
        .build()

    val retrofit = Retrofit.Builder()
        .baseUrl("https://www.westpac.com.au/bin/")
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    retrofit.create(Network::class.java)
}

fun getNetworkService() = service

interface Network {

    @GET("getJsonRates.wbc.fx.json")
    suspend fun  fetchDetails():Json4Kotlin_Base


}