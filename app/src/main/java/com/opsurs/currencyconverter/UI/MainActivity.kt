package com.opsurs.currencyconverter.UI

import AED
import ARS
import BDT
import BND
import BRL
import Brands
import CAD
import CHF
import CLP
import CNH
import CNY
import CurrencyData
import DKK
import EUR
import FJD
import FX
import GBP
import HKD
import IDR
import INR
import JPY
import KRW
import LKR
import MYR
import NOK
import NZD
import PGK
import PHP
import PKR
import Portfolios
import Products
import SAR
import SBD
import SEK
import SGD
import THB
import TOP
import TWD
import USD
import VND
import VUV
import WBC
import WST
import XAU
import XPF
import ZAR
import android.content.Context
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.MenuRes
import androidx.appcompat.widget.PopupMenu
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.opsurs.currencyconverter.Network.getNetworkService
import com.opsurs.currencyconverter.R
import com.opsurs.currencyconverter.Repository.CurrencyDetailsRepo
import com.opsurs.currencyconverter.ViewModel.CurrencyDetailVM
import com.opsurs.currencyconverter.databinding.ActivityMainBinding
import java.lang.NumberFormatException

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var products: Products
    var sellTT: Double = 0.0
    var buyTT: Double = 0.0
    var buyTC: Double = 0.0
    var menuSelected: Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        binding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_main
        )
        binding.setLifecycleOwner(this)

        val inputMethodManager =
            applicationContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(binding.mainView.windowToken, 0)


        // Network call
        val repository = CurrencyDetailsRepo(getNetworkService())
        val viewModel = ViewModelProviders
            .of(
                this, CurrencyDetailVM.FACTORY(
                    repository
                )
            ).get(CurrencyDetailVM::class.java)

        viewModel.onGetCurrencyDetail()

        // observe livedata

        viewModel.mutableCurrencyLiveData.observe(this) { value ->
            value?.let {
                Log.i("CurrencyDetailResponse", value.apiVersion.toString())

                val currencyData: CurrencyData = value.data
                val brands: Brands = currencyData.Brands
                val wbc: WBC = brands.WBC
                val portfolios: Portfolios = wbc.Portfolios
                val fx: FX = portfolios.FX
                products = fx.Products

                try {
                    callProducts()
                } catch (e: Exception) {
                    val snack = Snackbar.make(binding.mainView, R.string.network_error,
                        Snackbar.LENGTH_LONG)
                    snack.view.setBackgroundColor(Color.RED)
                    snack.show()
                }


            }
        }

        // designed material popup menu for currency
        binding.popCurrency.setOnClickListener { v: View ->
            if(::products.isInitialized){
            showMenu(v, R.menu.popup_menu)}
            else{
                val snack = Snackbar.make(binding.mainView, R.string.network_error,
                    Snackbar.LENGTH_LONG)
                snack.view.setBackgroundColor(Color.RED)
                snack.show()
            }

        }

        binding.otherCurrencyVal.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                if (!binding.otherCurrencyVal.equals("")) {
                    callCurrencyConverter(sellTT, buyTT, buyTC)
                }
            }

        })


    }

    // check to get the right values from the product data class
    fun callProducts() {
        val usd: USD = products.USD
        val eur: EUR = products.EUR
        val gbp: GBP = products.GBP
        val nzd: NZD = products.NZD
        val cny: CNY = products.CNY
        val aed: AED = products.AED
        val ars: ARS = products.ARS
        val bdt: BDT = products.BDT
        val bnd: BND = products.BND
        val brl: BRL = products.BRL
        val cad: CAD = products.CAD
        val chf: CHF = products.CHF
        val clp: CLP = products.CLP
        val dkk: DKK = products.DKK
        val fjd: FJD = products.FJD
        val hkd: HKD = products.HKD
        val idr: IDR = products.IDR
        val inr: INR = products.INR
        val jpy: JPY = products.JPY
        val krw: KRW = products.KRW
        val lkr: LKR = products.LKR
        val myr: MYR = products.MYR
        val nok: NOK = products.NOK
        val pgk: PGK = products.PGK
        val php: PHP = products.PHP
        val pkr: PKR = products.PKR
        val sar: SAR = products.SAR
        val sbd: SBD = products.SBD
        val sek: SEK = products.SEK
        val sgd: SGD = products.SGD
        val thb: THB = products.THB
        val top: TOP = products.TOP
        val twd: TWD = products.TWD
        val vnd: VND = products.VND
        val vuv: VUV = products.VUV
        val wst: WST = products.WST
        val xpf: XPF = products.XPF
        val zar: ZAR = products.ZAR
        val xau: XAU = products.XAU
        val cnh: CNH = products.CNH


        Log.i("USDRate", usd.Rates.USD.country)
        Log.i("USDRate", eur.Rates.EUR.country)
        Log.i("USDRate", gbp.Rates.GBP.country)
        Log.i("USDRate", nzd.Rates.NZD.country)
        Log.i("USDRate", cny.Rates.CNY.country)
        Log.i("USDRate", aed.Rates.AED.country)
        Log.i("USDRate", ars.Rates.ARS.country)
        Log.i("USDRate", bdt.Rates.BDT.country)
        Log.i("USDRate", bnd.Rates.BND.country)
        Log.i("USDRate", brl.Rates.BRL.country)
        Log.i("USDRate", cad.Rates.CAD.country)
        Log.i("USDRate", chf.Rates.CHF.country)
        Log.i("USDRate", clp.Rates.CLP.country)
        Log.i("USDRate", dkk.Rates.DKK.country)
        Log.i("USDRate", fjd.Rates.FJD.country)
        Log.i("USDRate", hkd.Rates.HKD.country)
        Log.i("USDRate", idr.Rates.IDR.country)
        Log.i("USDRate", inr.Rates.INR.country)
        Log.i("USDRate", jpy.Rates.JPY.country)
        Log.i("USDRate", krw.Rates.KRW.country)
        Log.i("USDRate", lkr.Rates.LKR.country)
        Log.i("USDRate", myr.Rates.MYR.country)
        Log.i("USDRate", nok.Rates.NOK.country)
        Log.i("USDRate", pgk.Rates.PGK.country)
        Log.i("USDRate", php.Rates.PHP.country)
        Log.i("USDRate", pkr.Rates.PKR.country)
        Log.i("USDRate", sar.Rates.SAR.country)
        Log.i("USDRate", sbd.Rates.SBD.country)
        Log.i("USDRate", sek.Rates.SEK.country)
        Log.i("USDRate", sgd.Rates.SGD.country)
        Log.i("USDRate", thb.Rates.THB.country)
        Log.i("USDRate", top.Rates.TOP.country)
        Log.i("USDRate", twd.Rates.TWD.country)
        Log.i("USDRate", vnd.Rates.VND.country)
        Log.i("USDRate", vuv.Rates.VUV.country)
        Log.i("USDRate", wst.Rates.WST.country)
        Log.i("USDRate", xpf.Rates.XPF.country)
        Log.i("USDRate", zar.Rates.ZAR.country)
        Log.i("USDRate", xau.Rates.XAU.country)
        Log.i("USDRate", cnh.Rates.CNH.country)


    }

    private fun showMenu(v: View, @MenuRes menuRes: Int) {

        val popup = PopupMenu(this, v)
        popup.menuInflater.inflate(menuRes, popup.menu)

        popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
            override fun onMenuItemClick(item: MenuItem): Boolean {
                menuSelected = true

                when (item.itemId) {
                    R.id.usd -> {
                        binding.popCurrency.text =
                            resources.getString(R.string.usd)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.USD.Rates.USD.sellTT.equals("On App") && !products.USD.Rates.USD.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.USD.Rates.USD.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.USD.Rates.USD.sellTT
                        }

                        if (!products.USD.Rates.USD.buyTT.equals("On App") && !products.USD.Rates.USD.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.USD.Rates.USD.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.USD.Rates.USD.buyTT
                        }

                        if (!products.USD.Rates.USD.buyTC.equals("On App") && !products.USD.Rates.USD.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.USD.Rates.USD.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.USD.Rates.USD.buyTC
                        }

                        binding.updated.text = "updated On ${products.USD.Rates.USD.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.USD.Rates.USD.country
                        binding.currencyName.text = products.USD.Rates.USD.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }

                        return true
                    }
                    R.id.eur -> {

                        binding.popCurrency.text =
                            resources.getString(R.string.eur)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.EUR.Rates.EUR.sellTT.equals("On App") && !products.EUR.Rates.EUR.sellTT.equals(
                                "N/A")
                        ) {
                            sellTT = products.EUR.Rates.EUR.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.EUR.Rates.EUR.sellTT
                        }

                        if (!products.EUR.Rates.EUR.buyTT.equals("On App") && !products.EUR.Rates.EUR.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.EUR.Rates.EUR.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.EUR.Rates.EUR.buyTT

                        }

                        if (!products.EUR.Rates.EUR.buyTC.equals("On App") && !products.EUR.Rates.EUR.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.EUR.Rates.EUR.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.EUR.Rates.EUR.buyTC

                        }

                        binding.updated.text = "updated On ${products.EUR.Rates.EUR.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.EUR.Rates.EUR.country
                        binding.currencyName.text = products.EUR.Rates.EUR.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }

                        return true
                    }
                    R.id.gbp -> {
                        binding.popCurrency.text =
                            resources.getString(R.string.GBP)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.GBP.Rates.GBP.sellTT.equals("On App") && !products.GBP.Rates.GBP.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.GBP.Rates.GBP.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.GBP.Rates.GBP.sellTT
                        }

                        if (!products.GBP.Rates.GBP.buyTT.equals("On App") && !products.GBP.Rates.GBP.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.GBP.Rates.GBP.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.GBP.Rates.GBP.buyTT
                        }

                        if (!products.GBP.Rates.GBP.buyTC.equals("On App") && !products.GBP.Rates.GBP.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.GBP.Rates.GBP.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.GBP.Rates.GBP.buyTC
                        }

                        binding.updated.text = "updated On ${products.GBP.Rates.GBP.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.GBP.Rates.GBP.country
                        binding.currencyName.text = products.GBP.Rates.GBP.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }

                    R.id.nzd -> {
                        binding.popCurrency.text =
                            resources.getString(R.string.NZD)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.NZD.Rates.NZD.sellTT.equals("On App") && !products.NZD.Rates.NZD.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.NZD.Rates.NZD.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.NZD.Rates.NZD.sellTT
                        }

                        if (!products.NZD.Rates.NZD.buyTT.equals("On App") && !products.NZD.Rates.NZD.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.NZD.Rates.NZD.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.NZD.Rates.NZD.buyTT
                        }

                        if (!products.NZD.Rates.NZD.buyTC.equals("On App") && !products.NZD.Rates.NZD.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.NZD.Rates.NZD.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.NZD.Rates.NZD.buyTC
                        }

                        binding.updated.text = "updated On ${products.NZD.Rates.NZD.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.NZD.Rates.NZD.country
                        binding.currencyName.text = products.NZD.Rates.NZD.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }
                    R.id.cny -> {
                        binding.popCurrency.text =
                            resources.getString(R.string.CNY)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.CNY.Rates.CNY.sellTT.equals("On App") && !products.CNY.Rates.CNY.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.CNY.Rates.CNY.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.CNY.Rates.CNY.sellTT
                        }

                        if (!products.CNY.Rates.CNY.buyTT.equals("On App") && !products.CNY.Rates.CNY.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.CNY.Rates.CNY.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.CNY.Rates.CNY.buyTT
                        }

                        if (!products.CNY.Rates.CNY.buyTC.equals("On App") && !products.CNY.Rates.CNY.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.CNY.Rates.CNY.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.CNY.Rates.CNY.buyTC
                        }

                        binding.updated.text = "updated On ${products.CNY.Rates.CNY.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.CNY.Rates.CNY.country
                        binding.currencyName.text = products.CNY.Rates.CNY.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }
                    R.id.aed -> {
                        binding.popCurrency.text =
                            resources.getString(R.string.AED)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.AED.Rates.AED.sellTT.equals("On App") && !products.AED.Rates.AED.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.AED.Rates.AED.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.AED.Rates.AED.sellTT
                        }

                        if (!products.AED.Rates.AED.buyTT.equals("On App") && !products.AED.Rates.AED.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.AED.Rates.AED.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.AED.Rates.AED.buyTT
                        }

                        if (!products.AED.Rates.AED.buyTC.equals("On App") && !products.AED.Rates.AED.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.AED.Rates.AED.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.AED.Rates.AED.buyTC
                        }

                        binding.updated.text = "updated On ${products.AED.Rates.AED.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.AED.Rates.AED.country
                        binding.currencyName.text = products.AED.Rates.AED.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }
                    R.id.ars -> {
                        binding.popCurrency.text =
                            resources.getString(R.string.ARS)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.ARS.Rates.ARS.sellTT.equals("On App") && !products.ARS.Rates.ARS.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.ARS.Rates.ARS.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.ARS.Rates.ARS.sellTT
                        }

                        if (!products.ARS.Rates.ARS.buyTT.equals("On App") && !products.ARS.Rates.ARS.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.ARS.Rates.ARS.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.ARS.Rates.ARS.buyTT
                        }

                        if (!products.ARS.Rates.ARS.buyTC.equals("On App") && !products.ARS.Rates.ARS.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.ARS.Rates.ARS.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.ARS.Rates.ARS.buyTC
                        }


                        binding.updated.text = "updated On ${products.ARS.Rates.ARS.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.ARS.Rates.ARS.country
                        binding.currencyName.text = products.ARS.Rates.ARS.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }
                    R.id.bdt -> {

                        binding.popCurrency.text =
                            resources.getString(R.string.BDT)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.BDT.Rates.BDT.sellTT.equals("On App") && !products.BDT.Rates.BDT.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.BDT.Rates.BDT.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.BDT.Rates.BDT.sellTT
                        }

                        if (!products.BDT.Rates.BDT.buyTT.equals("On App") && !products.BDT.Rates.BDT.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.BDT.Rates.BDT.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.BDT.Rates.BDT.buyTT
                        }


                        if (!products.BDT.Rates.BDT.buyTC.equals("On App") && !products.BDT.Rates.BDT.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.BDT.Rates.BDT.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.BDT.Rates.BDT.buyTC
                        }

                        binding.updated.text = "updated On ${products.BDT.Rates.BDT.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.BDT.Rates.BDT.country
                        binding.currencyName.text = products.BDT.Rates.BDT.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }
                    R.id.bnd -> {

                        binding.popCurrency.text =
                            resources.getString(R.string.BND)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.BND.Rates.BND.sellTT.equals("On App") && !products.BND.Rates.BND.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.BND.Rates.BND.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.BND.Rates.BND.sellTT
                        }

                        if (!products.BND.Rates.BND.buyTT.equals("On App") && !products.BND.Rates.BND.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.BND.Rates.BND.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.BND.Rates.BND.buyTT
                        }

                        if (!products.BND.Rates.BND.buyTC.equals("On App") && !products.BND.Rates.BND.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.BND.Rates.BND.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.BND.Rates.BND.buyTC
                        }


                        binding.updated.text = "updated On ${products.BND.Rates.BND.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.BND.Rates.BND.country
                        binding.currencyName.text = products.BND.Rates.BND.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }
                    R.id.brl -> {
                        binding.popCurrency.text =
                            resources.getString(R.string.BRL)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.BRL.Rates.BRL.sellTT.equals("On App") && !products.BRL.Rates.BRL.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.BRL.Rates.BRL.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.BRL.Rates.BRL.sellTT
                        }

                        if (!products.BRL.Rates.BRL.buyTT.equals("On App") && !products.BRL.Rates.BRL.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.BRL.Rates.BRL.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.BRL.Rates.BRL.buyTT
                        }

                        if (!products.BRL.Rates.BRL.buyTC.equals("On App") && !products.BRL.Rates.BRL.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.BRL.Rates.BRL.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.BRL.Rates.BRL.buyTC
                        }

                        binding.updated.text = "updated On ${products.BRL.Rates.BRL.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.BRL.Rates.BRL.country
                        binding.currencyName.text = products.BRL.Rates.BRL.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }

                        return true
                    }
                    R.id.cad -> {
                        binding.popCurrency.text =
                            resources.getString(R.string.CAD)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.CAD.Rates.CAD.sellTT.equals("On App") && !products.CAD.Rates.CAD.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.CAD.Rates.CAD.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.CAD.Rates.CAD.sellTT
                        }

                        if (!products.CAD.Rates.CAD.buyTT.equals("On App") && !products.CAD.Rates.CAD.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.CAD.Rates.CAD.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.CAD.Rates.CAD.buyTT
                        }

                        if (!products.CAD.Rates.CAD.buyTC.equals("On App") && !products.CAD.Rates.CAD.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.CAD.Rates.CAD.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.CAD.Rates.CAD.buyTC
                        }

                        binding.updated.text = "updated On ${products.CAD.Rates.CAD.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.CAD.Rates.CAD.country
                        binding.currencyName.text = products.CAD.Rates.CAD.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }
                    R.id.chf -> {
                        binding.popCurrency.text =
                            resources.getString(R.string.CHF)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.CHF.Rates.CHF.sellTT.equals("On App") && !products.CHF.Rates.CHF.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.CHF.Rates.CHF.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.CHF.Rates.CHF.sellTT
                        }

                        if (!products.CHF.Rates.CHF.buyTT.equals("On App") && !products.CHF.Rates.CHF.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.CHF.Rates.CHF.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.CHF.Rates.CHF.buyTT
                        }

                        if (!products.CHF.Rates.CHF.buyTC.equals("On App") && !products.CHF.Rates.CHF.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.CHF.Rates.CHF.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.CHF.Rates.CHF.buyTC
                        }

                        binding.updated.text = "updated On ${products.CHF.Rates.CHF.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.CHF.Rates.CHF.country
                        binding.currencyName.text = products.CHF.Rates.CHF.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }
                    R.id.clp -> {
                        binding.popCurrency.text =
                            resources.getString(R.string.CLP)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.CLP.Rates.CLP.sellTT.equals("On App") && !products.CLP.Rates.CLP.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.CLP.Rates.CLP.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.CLP.Rates.CLP.sellTT
                        }

                        if (!products.CLP.Rates.CLP.buyTT.equals("On App") && !products.CLP.Rates.CLP.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.CLP.Rates.CLP.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.CLP.Rates.CLP.buyTT
                        }

                        if (!products.CLP.Rates.CLP.buyTC.equals("On App") && !products.CLP.Rates.CLP.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.CLP.Rates.CLP.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.CLP.Rates.CLP.buyTC
                        }

                        binding.updated.text = "updated On ${products.CLP.Rates.CLP.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.CLP.Rates.CLP.country
                        binding.currencyName.text = products.CLP.Rates.CLP.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }
                    R.id.dkk -> {

                        binding.popCurrency.text =
                            resources.getString(R.string.DKK)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.DKK.Rates.DKK.sellTT.equals("On App") && !products.DKK.Rates.DKK.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.DKK.Rates.DKK.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.DKK.Rates.DKK.sellTT
                        }

                        if (!products.DKK.Rates.DKK.buyTT.equals("On App") && !products.DKK.Rates.DKK.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.DKK.Rates.DKK.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.DKK.Rates.DKK.buyTT
                        }

                        if (!products.DKK.Rates.DKK.buyTC.equals("On App") && !products.DKK.Rates.DKK.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.DKK.Rates.DKK.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.DKK.Rates.DKK.buyTC
                        }

                        binding.updated.text = "updated On ${products.DKK.Rates.DKK.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.DKK.Rates.DKK.country
                        binding.currencyName.text = products.DKK.Rates.DKK.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }
                    R.id.fjd -> {

                        binding.popCurrency.text =
                            resources.getString(R.string.FJD)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.FJD.Rates.FJD.sellTT.equals("On App") && !products.FJD.Rates.FJD.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.FJD.Rates.FJD.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.FJD.Rates.FJD.sellTT
                        }

                        if (!products.FJD.Rates.FJD.buyTT.equals("On App") && !products.FJD.Rates.FJD.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.FJD.Rates.FJD.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.FJD.Rates.FJD.buyTT
                        }

                        if (!products.FJD.Rates.FJD.buyTC.equals("On App") && !products.FJD.Rates.FJD.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.FJD.Rates.FJD.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.FJD.Rates.FJD.buyTC
                        }

                        binding.updated.text = "updated On ${products.FJD.Rates.FJD.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.FJD.Rates.FJD.country
                        binding.currencyName.text = products.FJD.Rates.FJD.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }
                    R.id.hkd -> {

                        binding.popCurrency.text =
                            resources.getString(R.string.HKD)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.HKD.Rates.HKD.sellTT.equals("On App") && !products.HKD.Rates.HKD.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.HKD.Rates.HKD.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.HKD.Rates.HKD.sellTT
                        }

                        if (!products.HKD.Rates.HKD.buyTT.equals("On App") && !products.HKD.Rates.HKD.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.HKD.Rates.HKD.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.HKD.Rates.HKD.buyTT
                        }

                        if (!products.HKD.Rates.HKD.buyTC.equals("On App") && !products.HKD.Rates.HKD.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.HKD.Rates.HKD.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.HKD.Rates.HKD.buyTC
                        }


                        binding.updated.text = "updated On ${products.HKD.Rates.HKD.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.HKD.Rates.HKD.country
                        binding.currencyName.text = products.HKD.Rates.HKD.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }
                    R.id.idr -> {

                        binding.popCurrency.text =
                            resources.getString(R.string.IDR)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.IDR.Rates.IDR.sellTT.equals("On App") && !products.IDR.Rates.IDR.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.IDR.Rates.IDR.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.IDR.Rates.IDR.sellTT
                        }


                        if (!products.IDR.Rates.IDR.buyTT.equals("On App") && !products.IDR.Rates.IDR.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.IDR.Rates.IDR.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.IDR.Rates.IDR.buyTT
                        }

                        if (!products.IDR.Rates.IDR.buyTC.equals("On App") && !products.IDR.Rates.IDR.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.IDR.Rates.IDR.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.IDR.Rates.IDR.buyTC
                        }

                        binding.updated.text = "updated On ${products.IDR.Rates.IDR.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.IDR.Rates.IDR.country
                        binding.currencyName.text = products.IDR.Rates.IDR.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }

                        return true
                    }
                    R.id.inr -> {
                        binding.popCurrency.text =
                            resources.getString(R.string.INR)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.INR.Rates.INR.sellTT.equals("On App") && !products.INR.Rates.INR.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.INR.Rates.INR.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.INR.Rates.INR.sellTT
                        }
                        if (!products.INR.Rates.INR.buyTT.equals("On App") && !products.INR.Rates.INR.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.INR.Rates.INR.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.INR.Rates.INR.buyTT
                        }
                        if (!products.INR.Rates.INR.buyTC.equals("On App") && !products.INR.Rates.INR.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.INR.Rates.INR.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.INR.Rates.INR.buyTC
                        }
                        binding.updated.text = "updated On ${products.INR.Rates.INR.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.INR.Rates.INR.country
                        binding.currencyName.text = products.INR.Rates.INR.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }
                    R.id.jpy -> {
                        binding.popCurrency.text =
                            resources.getString(R.string.JPY)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.JPY.Rates.JPY.sellTT.equals("On App") && !products.JPY.Rates.JPY.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.JPY.Rates.JPY.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.JPY.Rates.JPY.sellTT
                        }

                        if (!products.JPY.Rates.JPY.buyTT.equals("On App") && !products.JPY.Rates.JPY.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.JPY.Rates.JPY.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.JPY.Rates.JPY.buyTT
                        }

                        if (!products.JPY.Rates.JPY.buyTC.equals("On App") && !products.JPY.Rates.JPY.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.JPY.Rates.JPY.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.JPY.Rates.JPY.buyTC
                        }

                        binding.updated.text = "updated On ${products.JPY.Rates.JPY.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.JPY.Rates.JPY.country
                        binding.currencyName.text = products.JPY.Rates.JPY.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }
                    R.id.krw -> {
                        binding.popCurrency.text =
                            resources.getString(R.string.KRW)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.KRW.Rates.KRW.sellTT.equals("On App") && !products.KRW.Rates.KRW.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.KRW.Rates.KRW.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.KRW.Rates.KRW.sellTT
                        }

                        if (!products.KRW.Rates.KRW.buyTT.equals("On App") && !products.KRW.Rates.KRW.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.KRW.Rates.KRW.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.KRW.Rates.KRW.buyTT
                        }

                        if (!products.KRW.Rates.KRW.buyTC.equals("On App") && !products.KRW.Rates.KRW.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.KRW.Rates.KRW.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.KRW.Rates.KRW.buyTC
                        }

                        binding.updated.text = "updated On ${products.KRW.Rates.KRW.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.KRW.Rates.KRW.country
                        binding.currencyName.text = products.KRW.Rates.KRW.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }
                    R.id.lkr -> {

                        binding.popCurrency.text =
                            resources.getString(R.string.LKR)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.LKR.Rates.LKR.sellTT.equals("On App") && !products.LKR.Rates.LKR.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.LKR.Rates.LKR.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.LKR.Rates.LKR.sellTT
                        }

                        if (!products.LKR.Rates.LKR.buyTT.equals("On App") && !products.LKR.Rates.LKR.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.LKR.Rates.LKR.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.LKR.Rates.LKR.buyTT
                        }

                        if (!products.LKR.Rates.LKR.buyTC.equals("On App") && !products.LKR.Rates.LKR.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.LKR.Rates.LKR.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.LKR.Rates.LKR.buyTC
                        }

                        binding.updated.text = "updated On ${products.LKR.Rates.LKR.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.LKR.Rates.LKR.country
                        binding.currencyName.text = products.LKR.Rates.LKR.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }
                    R.id.myr -> {

                        binding.popCurrency.text =
                            resources.getString(R.string.MYR)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.MYR.Rates.MYR.sellTT.equals("On App") && !products.MYR.Rates.MYR.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.MYR.Rates.MYR.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.MYR.Rates.MYR.sellTT
                        }

                        if (!products.MYR.Rates.MYR.buyTT.equals("On App") && !products.MYR.Rates.MYR.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.MYR.Rates.MYR.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.MYR.Rates.MYR.buyTT
                        }

                        if (!products.MYR.Rates.MYR.buyTC.equals("On App") && !products.MYR.Rates.MYR.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.MYR.Rates.MYR.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.MYR.Rates.MYR.buyTC
                        }

                        binding.updated.text = "updated On ${products.MYR.Rates.MYR.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.MYR.Rates.MYR.country
                        binding.currencyName.text = products.MYR.Rates.MYR.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }
                    R.id.nok -> {

                        binding.popCurrency.text =
                            resources.getString(R.string.NOK)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.NOK.Rates.NOK.sellTT.equals("On App") && !products.NOK.Rates.NOK.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.NOK.Rates.NOK.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.NOK.Rates.NOK.sellTT
                        }

                        if (!products.NOK.Rates.NOK.buyTT.equals("On App") && !products.NOK.Rates.NOK.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.NOK.Rates.NOK.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.NOK.Rates.NOK.buyTT
                        }

                        if (!products.NOK.Rates.NOK.buyTC.equals("On App") && !products.NOK.Rates.NOK.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.NOK.Rates.NOK.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.NOK.Rates.NOK.buyTC
                        }

                        binding.updated.text = "updated On ${products.NOK.Rates.NOK.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.NOK.Rates.NOK.country
                        binding.currencyName.text = products.NOK.Rates.NOK.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }
                    R.id.pgk -> {

                        binding.popCurrency.text =
                            resources.getString(R.string.PGK)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.PGK.Rates.PGK.sellTT.equals("On App") && !products.PGK.Rates.PGK.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.PGK.Rates.PGK.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.PGK.Rates.PGK.sellTT
                        }

                        if (!products.PGK.Rates.PGK.buyTT.equals("On App") && !products.PGK.Rates.PGK.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.PGK.Rates.PGK.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.PGK.Rates.PGK.buyTT
                        }

                        if (!products.PGK.Rates.PGK.buyTC.equals("On App") && !products.PGK.Rates.PGK.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.PGK.Rates.PGK.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.PGK.Rates.PGK.buyTC
                        }

                        binding.updated.text = "updated On ${products.PGK.Rates.PGK.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.PGK.Rates.PGK.country
                        binding.currencyName.text = products.PGK.Rates.PGK.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }
                    R.id.php -> {

                        binding.popCurrency.text =
                            resources.getString(R.string.PHP)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.PHP.Rates.PHP.sellTT.equals("On App") && !products.PHP.Rates.PHP.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.PHP.Rates.PHP.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.PHP.Rates.PHP.sellTT
                        }

                        if (!products.PHP.Rates.PHP.buyTT.equals("On App") && !products.PHP.Rates.PHP.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.PHP.Rates.PHP.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.PHP.Rates.PHP.buyTT
                        }

                        if (!products.PHP.Rates.PHP.buyTC.equals("On App") && !products.PHP.Rates.PHP.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.PHP.Rates.PHP.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.PHP.Rates.PHP.buyTC
                        }

                        binding.updated.text = "updated On ${products.PHP.Rates.PHP.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.PHP.Rates.PHP.country
                        binding.currencyName.text = products.PHP.Rates.PHP.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }
                    R.id.pkr -> {

                        binding.popCurrency.text =
                            resources.getString(R.string.PKR)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.PKR.Rates.PKR.sellTT.equals("On App") && !products.PKR.Rates.PKR.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.PKR.Rates.PKR.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.PKR.Rates.PKR.sellTT
                        }

                        if (!products.PKR.Rates.PKR.buyTT.equals("On App") && !products.PKR.Rates.PKR.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.PKR.Rates.PKR.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.PKR.Rates.PKR.buyTT
                        }

                        if (!products.PKR.Rates.PKR.buyTC.equals("On App") && !products.PKR.Rates.PKR.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.PKR.Rates.PKR.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.PKR.Rates.PKR.buyTC
                        }

                        binding.updated.text = "updated On ${products.PKR.Rates.PKR.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.PKR.Rates.PKR.country
                        binding.currencyName.text = products.PKR.Rates.PKR.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }

                    R.id.sar -> {
                        binding.popCurrency.text =
                            resources.getString(R.string.SAR)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.SAR.Rates.SAR.sellTT.equals("On App") && !products.SAR.Rates.SAR.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.SAR.Rates.SAR.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.SAR.Rates.SAR.sellTT
                        }

                        if (!products.SAR.Rates.SAR.buyTT.equals("On App") && !products.SAR.Rates.SAR.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.SAR.Rates.SAR.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.SAR.Rates.SAR.buyTT
                        }

                        if (!products.SAR.Rates.SAR.buyTC.equals("On App") && !products.SAR.Rates.SAR.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.SAR.Rates.SAR.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.SAR.Rates.SAR.buyTC
                        }

                        binding.updated.text = "updated On ${products.SAR.Rates.SAR.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.SAR.Rates.SAR.country
                        binding.currencyName.text = products.SAR.Rates.SAR.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }
                    R.id.sbd -> {

                        binding.popCurrency.text =
                            resources.getString(R.string.SBD)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.SBD.Rates.SBD.sellTT.equals("On App") && !products.SBD.Rates.SBD.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.SBD.Rates.SBD.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.SBD.Rates.SBD.sellTT
                        }

                        if (!products.SBD.Rates.SBD.buyTT.equals("On App") && !products.SBD.Rates.SBD.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.SBD.Rates.SBD.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.SBD.Rates.SBD.buyTT
                        }

                        if (!products.SBD.Rates.SBD.buyTC.equals("On App") && !products.SBD.Rates.SBD.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.SBD.Rates.SBD.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.SBD.Rates.SBD.buyTC
                        }

                        binding.updated.text = "updated On ${products.SBD.Rates.SBD.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.SBD.Rates.SBD.country
                        binding.currencyName.text = products.SBD.Rates.SBD.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }
                    R.id.sek -> {
                        binding.popCurrency.text =
                            resources.getString(R.string.SEK)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.SEK.Rates.SEK.sellTT.equals("On App") && !products.SEK.Rates.SEK.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.SEK.Rates.SEK.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.SEK.Rates.SEK.sellTT
                        }

                        if (!products.SEK.Rates.SEK.buyTT.equals("On App") && !products.SEK.Rates.SEK.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.SEK.Rates.SEK.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.SEK.Rates.SEK.buyTT
                        }

                        if (!products.SEK.Rates.SEK.buyTC.equals("On App") && !products.SEK.Rates.SEK.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.SEK.Rates.SEK.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.SEK.Rates.SEK.buyTC
                        }

                        binding.updated.text = "updated On ${products.SEK.Rates.SEK.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.SEK.Rates.SEK.country
                        binding.currencyName.text = products.SEK.Rates.SEK.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }
                    R.id.sgd -> {
                        binding.popCurrency.text =
                            resources.getString(R.string.SGD)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.SGD.Rates.SGD.sellTT.equals("On App") && !products.SGD.Rates.SGD.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.SGD.Rates.SGD.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.SGD.Rates.SGD.sellTT
                        }

                        if (!products.SGD.Rates.SGD.buyTT.equals("On App") && !products.SGD.Rates.SGD.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.SGD.Rates.SGD.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.SGD.Rates.SGD.buyTT
                        }

                        if (!products.SGD.Rates.SGD.buyTC.equals("On App") && !products.SGD.Rates.SGD.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.SGD.Rates.SGD.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.SGD.Rates.SGD.buyTC
                        }

                        binding.updated.text = "updated On ${products.SGD.Rates.SGD.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.SGD.Rates.SGD.country
                        binding.currencyName.text = products.SGD.Rates.SGD.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }
                    R.id.thb -> {
                        binding.popCurrency.text =
                            resources.getString(R.string.THB)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.THB.Rates.THB.sellTT.equals("On App") && !products.THB.Rates.THB.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.THB.Rates.THB.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.THB.Rates.THB.sellTT
                        }

                        if (!products.THB.Rates.THB.buyTT.equals("On App") && !products.THB.Rates.THB.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.THB.Rates.THB.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.THB.Rates.THB.buyTT
                        }

                        if (!products.THB.Rates.THB.buyTC.equals("On App") && !products.THB.Rates.THB.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.THB.Rates.THB.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.THB.Rates.THB.buyTC
                        }

                        binding.updated.text = "updated On ${products.THB.Rates.THB.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.THB.Rates.THB.country
                        binding.currencyName.text = products.THB.Rates.THB.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }
                    R.id.top -> {

                        binding.popCurrency.text =
                            resources.getString(R.string.TOP)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.TOP.Rates.TOP.sellTT.equals("On App") && !products.TOP.Rates.TOP.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.TOP.Rates.TOP.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.TOP.Rates.TOP.sellTT
                        }

                        if (!products.TOP.Rates.TOP.buyTT.equals("On App") && !products.TOP.Rates.TOP.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.TOP.Rates.TOP.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.TOP.Rates.TOP.buyTT
                        }

                        if (!products.TOP.Rates.TOP.buyTC.equals("On App") && !products.TOP.Rates.TOP.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.TOP.Rates.TOP.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.TOP.Rates.TOP.buyTC
                        }


                        binding.updated.text = "updated On ${products.TOP.Rates.TOP.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.TOP.Rates.TOP.country
                        binding.currencyName.text = products.TOP.Rates.TOP.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }

                    R.id.twd -> {
                        binding.popCurrency.text =
                            resources.getString(R.string.TWD)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.TWD.Rates.TWD.sellTT.equals("On App") && !products.TWD.Rates.TWD.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.TWD.Rates.TWD.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.TWD.Rates.TWD.sellTT
                        }

                        if (!products.TWD.Rates.TWD.buyTT.equals("On App") && !products.TWD.Rates.TWD.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.TWD.Rates.TWD.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.TWD.Rates.TWD.buyTT
                        }

                        if (!products.TWD.Rates.TWD.buyTC.equals("On App") && !products.TWD.Rates.TWD.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.TWD.Rates.TWD.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.TWD.Rates.TWD.buyTC
                        }

                        binding.updated.text = "updated On ${products.TWD.Rates.TWD.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.TWD.Rates.TWD.country
                        binding.currencyName.text = products.TWD.Rates.TWD.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }
                    R.id.vnd -> {
                        binding.popCurrency.text =
                            resources.getString(R.string.VND)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.VND.Rates.VND.sellTT.equals("On App") && !products.VND.Rates.VND.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.VND.Rates.VND.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.VND.Rates.VND.sellTT
                        }

                        if (!products.VND.Rates.VND.buyTT.equals("On App") && !products.VND.Rates.VND.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.VND.Rates.VND.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.VND.Rates.VND.buyTT
                        }

                        if (!products.VND.Rates.VND.buyTC.equals("On App") && !products.VND.Rates.VND.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.VND.Rates.VND.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.VND.Rates.VND.buyTC
                        }

                        binding.updated.text = "updated On ${products.VND.Rates.VND.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.VND.Rates.VND.country
                        binding.currencyName.text = products.VND.Rates.VND.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }
                    R.id.vuv -> {
                        binding.popCurrency.text =
                            resources.getString(R.string.VUV)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.VUV.Rates.VUV.sellTT.equals("On App") && !products.VUV.Rates.VUV.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.VUV.Rates.VUV.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.VUV.Rates.VUV.sellTT
                        }

                        if (!products.VUV.Rates.VUV.buyTT.equals("On App") && !products.VUV.Rates.VUV.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.VUV.Rates.VUV.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.VUV.Rates.VUV.buyTT
                        }

                        if (!products.VUV.Rates.VUV.buyTC.equals("On App") && !products.VUV.Rates.VUV.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.VUV.Rates.VUV.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.VUV.Rates.VUV.buyTC
                        }

                        binding.updated.text = "updated On ${products.VUV.Rates.VUV.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.VUV.Rates.VUV.country
                        binding.currencyName.text = products.VUV.Rates.VUV.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }
                    R.id.wst -> {
                        binding.popCurrency.text =
                            resources.getString(R.string.WST)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.WST.Rates.WST.sellTT.equals("On App") && !products.WST.Rates.WST.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.WST.Rates.WST.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.WST.Rates.WST.sellTT
                        }

                        if (!products.WST.Rates.WST.buyTT.equals("On App") && !products.WST.Rates.WST.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.WST.Rates.WST.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.WST.Rates.WST.buyTT
                        }

                        if (!products.WST.Rates.WST.buyTC.equals("On App") && !products.WST.Rates.WST.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.WST.Rates.WST.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.WST.Rates.WST.buyTC
                        }

                        binding.updated.text = "updated On ${products.WST.Rates.WST.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.WST.Rates.WST.country
                        binding.currencyName.text = products.WST.Rates.WST.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }

                    R.id.xpf -> {
                        binding.popCurrency.text =
                            resources.getString(R.string.XPF)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.XPF.Rates.XPF.sellTT.equals("On App") && !products.XPF.Rates.XPF.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.XPF.Rates.XPF.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.XPF.Rates.XPF.sellTT
                        }

                        if (!products.XPF.Rates.XPF.buyTT.equals("On App") && !products.XPF.Rates.XPF.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.XPF.Rates.XPF.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.XPF.Rates.XPF.buyTT
                        }

                        if (!products.XPF.Rates.XPF.buyTC.equals("On App") && !products.XPF.Rates.XPF.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.XPF.Rates.XPF.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.XPF.Rates.XPF.buyTC
                        }

                        binding.updated.text = "updated On ${products.XPF.Rates.XPF.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.XPF.Rates.XPF.country
                        binding.currencyName.text = products.XPF.Rates.XPF.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }
                    R.id.zar -> {
                        binding.popCurrency.text =
                            resources.getString(R.string.ZAR)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.ZAR.Rates.ZAR.sellTT.equals("On App") && !products.ZAR.Rates.ZAR.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.ZAR.Rates.ZAR.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.ZAR.Rates.ZAR.sellTT
                        }

                        if (!products.ZAR.Rates.ZAR.buyTT.equals("On App") && !products.ZAR.Rates.ZAR.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.ZAR.Rates.ZAR.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.ZAR.Rates.ZAR.buyTT
                        }

                        if (!products.ZAR.Rates.ZAR.buyTC.equals("On App") && !products.ZAR.Rates.ZAR.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.ZAR.Rates.ZAR.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.ZAR.Rates.ZAR.buyTC
                        }


                        binding.updated.text = "updated On ${products.ZAR.Rates.ZAR.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.ZAR.Rates.ZAR.country
                        binding.currencyName.text = products.ZAR.Rates.ZAR.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }
                    R.id.xau -> {
                        binding.popCurrency.text =
                            resources.getString(R.string.XAU)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.XAU.Rates.XAU.sellTT.equals("On App") && !products.XAU.Rates.XAU.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.XAU.Rates.XAU.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.XAU.Rates.XAU.sellTT
                        }

                        if (!products.XAU.Rates.XAU.buyTT.equals("On App") && !products.XAU.Rates.XAU.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.XAU.Rates.XAU.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.XAU.Rates.XAU.buyTT
                        }

                        if (!products.XAU.Rates.XAU.buyTC.equals("On App") && !products.XAU.Rates.XAU.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.XAU.Rates.XAU.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.XAU.Rates.XAU.buyTC
                        }

                        binding.updated.text = "updated On ${products.XAU.Rates.XAU.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.XAU.Rates.XAU.country
                        binding.currencyName.text = products.XAU.Rates.XAU.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }
                    R.id.cnh -> {
                        binding.popCurrency.text =
                            resources.getString(R.string.CNH)  // to bind on the selected value from dropdown to bitton text

                        // To bind the conversion detail values from network

                        if (!products.CNH.Rates.CNH.sellTT.equals("On App") && !products.CNH.Rates.CNH.sellTT.equals(
                                "N/A"
                            )
                        ) {
                            sellTT = products.CNH.Rates.CNH.sellTT.toDouble()
                        } else {
                            sellTT = 0.0
                            binding.audValue.text = products.CNH.Rates.CNH.sellTT
                        }

                        if (!products.CNH.Rates.CNH.buyTT.equals("On App") && !products.CNH.Rates.CNH.buyTT.equals(
                                "N/A"
                            )
                        ) {
                            buyTT = products.CNH.Rates.CNH.buyTT.toDouble()
                        } else {
                            buyTT = 0.0
                            binding.buyTT.text = products.CNH.Rates.CNH.buyTT
                        }

                        if (!products.CNH.Rates.CNH.buyTC.equals("On App") && !products.CNH.Rates.CNH.buyTC.equals(
                                "N/A"
                            )
                        ) {
                            buyTC = products.CNH.Rates.CNH.buyTC.toDouble()
                        } else {
                            buyTC = 0.0
                            binding.buyTc.text = products.CNH.Rates.CNH.buyTC
                        }

                        binding.updated.text = "updated On ${products.CNH.Rates.CNH.LASTUPDATED}"
                        binding.conversiondetailCard.visibility = View.VISIBLE
                        binding.country.text = products.CNH.Rates.CNH.country
                        binding.currencyName.text = products.CNH.Rates.CNH.currencyName

                        if (!binding.otherCurrencyVal.equals("")) {
                            callCurrencyConverter(sellTT, buyTT, buyTC)
                        }
                        return true
                    }
                    else -> return false
                }

            }
        })
        popup.setOnDismissListener {
            // Respond to popup being dismissed.
        }
        // Show the popup menu.
        popup.show()
    }


    // currency calculated for sellTT,buyTT,buyTC

    private fun callCurrencyConverter(sellTT: Double, buyTT: Double, buyTC: Double) {
        var aUDequals: Double = 0.0
        var aUDTTequals: Double = 0.0
        var aUDTCequals: Double = 0.0


        if (menuSelected) {
            if (binding.otherCurrencyVal.equals("")) {

                aUDequals = 0.0
                aUDTTequals = 0.0
                aUDTCequals = 0.0
            } else {

                try {
                    if (sellTT != 0.0) {
                        aUDequals = (binding.otherCurrencyVal.text.toString().toDouble()) / sellTT

                        val roundAUD = String.format("%.2f", aUDequals).toDouble()

                        binding.audValue.text = "${roundAUD.toString()} AUD"
                    }
                    if (buyTT != 0.0) {
                        aUDTTequals = (binding.otherCurrencyVal.text.toString().toDouble()) / buyTT

                        val roundTTAUD = String.format("%.2f", aUDTTequals).toDouble()

                        binding.buyTT.text = "${roundTTAUD.toString()} AUD"
                    }
                    if (buyTC != 0.0) {
                        aUDTCequals = (binding.otherCurrencyVal.text.toString().toDouble()) / buyTC

                        val roundTCAUD = String.format("%.2f", aUDTCequals).toDouble()

                        binding.buyTc.text = "${roundTCAUD.toString()} AUD"
                    }


                } catch (e: NumberFormatException) {
                }
            }
        } else {
            /*Toast.makeText(
                applicationContext,
                resources.getString(R.string.select_currency),
                Toast.LENGTH_LONG
            ).show()
*/
           val snack = Snackbar.make(binding.mainView, R.string.select_currency_snack,
                Snackbar.LENGTH_LONG)
                   snack.view.setBackgroundColor(Color.RED)
                    snack.show()
        }
    }
}
