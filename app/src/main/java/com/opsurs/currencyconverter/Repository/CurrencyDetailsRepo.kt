package com.opsurs.currencyconverter.Repository

import Json4Kotlin_Base
import com.opsurs.currencyconverter.Network.Network

class CurrencyDetailsRepo (val network: Network) {

    suspend fun refreshCurrencyDetail():Json4Kotlin_Base{
        val result:Json4Kotlin_Base
        try{
            result= network.fetchDetails()
        }
        catch(cause:Throwable){
            throw  CurrencyDetailRefreshError("Unable to fetch list",cause)

        }

        return result

    }


}


class CurrencyDetailRefreshError(message: String, cause: Throwable?) : Throwable(message, cause)