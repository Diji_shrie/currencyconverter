/* 
Copyright (c) 2021 Kotlin Data Classes Generated from JSON powered by http://www.json2kotlin.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */


data class Products (

	val USD : USD,
	val EUR : EUR,
	val GBP : GBP,
	val NZD : NZD,
	val CNY : CNY,
	val AED : AED,
	val ARS : ARS,
	val BDT : BDT,
	val BND : BND,
	val BRL : BRL,
	val CAD : CAD,
	val CHF : CHF,
	val CLP : CLP,
	val DKK : DKK,
	val FJD : FJD,
	val HKD : HKD,
	val IDR : IDR,
	val INR : INR,
	val JPY : JPY,
	val KRW : KRW,
	val LKR : LKR,
	val MYR : MYR,
	val NOK : NOK,
	val PGK : PGK,
	val PHP : PHP,
	val PKR : PKR,
	val SAR : SAR,
	val SBD : SBD,
	val SEK : SEK,
	val SGD : SGD,
	val THB : THB,
	val TOP : TOP,
	val TWD : TWD,
	val VND : VND,
	val VUV : VUV,
	val WST : WST,
	val XPF : XPF,
	val ZAR : ZAR,
	val XAU : XAU,
	val CNH : CNH
)