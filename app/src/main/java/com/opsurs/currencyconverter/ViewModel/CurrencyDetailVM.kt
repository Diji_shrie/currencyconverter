package com.opsurs.currencyconverter.ViewModel

import Json4Kotlin_Base
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.opsurs.currencyconverter.Repository.CurrencyDetailRefreshError
import com.opsurs.currencyconverter.Repository.CurrencyDetailsRepo
import com.opsurs.currencyconverter.Util.singleArgViewModelFactory
import kotlinx.coroutines.launch

class CurrencyDetailVM (private val repository: CurrencyDetailsRepo) : ViewModel() {

    val mutableCurrencyLiveData = MutableLiveData<Json4Kotlin_Base>()


    companion object {
        val FACTORY = singleArgViewModelFactory(::CurrencyDetailVM)
    }

    fun onGetCurrencyDetail(){
        getCurrencyDetail()
    }



    fun getCurrencyDetail(){
        viewModelScope.launch{
            try{
                mutableCurrencyLiveData.value = repository.refreshCurrencyDetail()
            }
            catch(error: CurrencyDetailRefreshError){


            }
            finally {

            }

        }
    }


}